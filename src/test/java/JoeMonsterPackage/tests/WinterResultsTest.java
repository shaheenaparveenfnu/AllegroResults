package JoeMonsterPackage.tests;

import JoeMonsterPackage.pages.GalleryPage;
import JoeMonsterPackage.pages.HomePage;
import MowerResultsPackage.utils.OsUtils;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by Ja on 06.02.2017.
 */
public class WinterResultsTest {
    protected OsUtils.OSType ostype;
    protected File tempFile;
    private WebDriver driver;
    private HomePage homePage;
    private GalleryPage galleryPage;

    private final static String URL = "http://joemonster.pl";
    private final static String WINTER_SEARCH_QUERY = "zimowa";

    @Before
    public void openBrowser()throws IOException {
        tempFile = File.createTempFile("tmpFile", "tmpFile1");
        ostype = OsUtils.getOperatingSystemType();
        switch (ostype) {
            case Windows:
                FileUtils.copyInputStreamToFile(getClass().getResourceAsStream("/chromedriverwin.exe"), tempFile);
                break;
            case MacOS:
                FileUtils.copyInputStreamToFile(getClass().getResourceAsStream("/chromedrivermac"), tempFile);
                tempFile.setExecutable(true);
                break;
            case Linux:
                FileUtils.copyInputStreamToFile(getClass().getResourceAsStream("/chromedriverlin"), tempFile);
                tempFile.setExecutable(true);
                break;
            case Other: break;
        }
        System.setProperty("webdriver.chrome.driver", tempFile.getAbsolutePath());
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(4, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        homePage = new HomePage(driver);
        homePage.open(URL);
    }
    @Test
    public void getWinterResults() throws InterruptedException,IOException{
        homePage.goToGallery();
        galleryPage = new GalleryPage(driver);
        galleryPage.searchWinterContent(WINTER_SEARCH_QUERY);
        galleryPage.getQueryResultsAndSave();
    }
    @After
    public void closeBrowser(){
       //driver.quit();
    }
}
