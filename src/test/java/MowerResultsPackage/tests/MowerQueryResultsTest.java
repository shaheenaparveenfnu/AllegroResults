package MowerResultsPackage.tests;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import MowerResultsPackage.pages.Homepage;
import MowerResultsPackage.utils.OsUtils;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class MowerQueryResultsTest {
    protected OsUtils.OSType ostype;
    protected File tempFile;
    private WebDriver driver;
    private Homepage homePage;

    private final static String URL = "http://allegro.pl";
    private final static String SEARCH_QUERY = "kosiarka do trawy";
    private final static String PRICE_FROM = "500";
    private final static String PRICE_TO = "4000";
    private final static String VOIVODESHIP = "z mazowieckiego";

    @Before
    public void openBrowser()throws IOException {
        tempFile = File.createTempFile("tmpFile", "tmpFile1");
        ostype = OsUtils.getOperatingSystemType();
        switch (ostype) {
            case Windows:
                FileUtils.copyInputStreamToFile(getClass().getResourceAsStream("/chromedriverwin.exe"), tempFile);
                break;
            case MacOS:
                FileUtils.copyInputStreamToFile(getClass().getResourceAsStream("/chromedrivermac"), tempFile);
                tempFile.setExecutable(true);
                break;
            case Linux:
                FileUtils.copyInputStreamToFile(getClass().getResourceAsStream("/chromedriverlin"), tempFile);
                tempFile.setExecutable(true);
                break;
            case Other: break;
        }
        System.setProperty("webdriver.chrome.driver", tempFile.getAbsolutePath());
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(4, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        homePage = new Homepage(driver);
        homePage.open(URL);
    }
    @Test
    public void getMowerQueryResults() throws InterruptedException,IOException{
        homePage.searchMower(SEARCH_QUERY);
        homePage.chooseCategory();
        homePage.filterQueryResult(PRICE_FROM,PRICE_TO,VOIVODESHIP);
        homePage.getQueryResults();
    }
    @After
    public void closeBrowser(){
        driver.quit();
    }
}
