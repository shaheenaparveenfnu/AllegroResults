package MowerResultsPackage.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import MowerResultsPackage.utils.WriteFileUtil;
import java.io.IOException;
import java.util.List;
import static org.junit.Assert.assertTrue;

public class Homepage {
    protected WebDriver driver;

    private static final String SEARCH_INPUT = "main-search-text";
    private static final String CHOOSE_CATEGORY = "//li/span[not(contains(., ' ciągnik'))]/..//span/strong[text()='kosiarka']/../strong[text()='do']/../strong[text()='trawy']/../../em/b[text() = 'Dom i Ogród']";
    private static final String NEW_MOWERS = "//span[text()='nowe']";
    private static final String BUY_NOW = "//span[text()='kup teraz']";
    private static final String PRICE_FROM = "price_from";
    private static final String PRICE_TO = "price_to";
    private static final String VOIVODESHIP = "parameter-state";

    @FindBy(id=SEARCH_INPUT)
    protected WebElement searchInput;

    @FindBy(xpath=CHOOSE_CATEGORY)
    protected WebElement chCategory;

    @FindBy(xpath=NEW_MOWERS)
    protected WebElement newItems;

    @FindBy(xpath=BUY_NOW)
    protected WebElement buyNowItems;

    @FindBy(id=PRICE_FROM)
    protected WebElement priceFromInput;

    @FindBy(id=PRICE_TO)
    protected WebElement priceToInput;

    @FindBy(id=VOIVODESHIP)
    protected WebElement selectVoivodeship;

    public Homepage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void open(String url){
        driver.get(url);
    }
    public void searchMower(String sQuery){
        validateDisplayedItems(searchInput);
        searchInput.click();
        searchInput.sendKeys(sQuery);
    }
    public void chooseCategory(){
        validateDisplayedItems(chCategory);
        chCategory.click();
    }
    public void filterQueryResult(String priceFromStr, String priceToStr, String voivodeship ) throws InterruptedException{
        validateDisplayedItems(newItems);
        newItems.click();
        validateDisplayedItems(buyNowItems);
        buyNowItems.click();
        validateDisplayedItems(selectVoivodeship);
        Select dropdown = new Select(driver.findElement(By.id("parameter-state")));
        dropdown.selectByVisibleText(voivodeship);
        validateDisplayedItems(priceFromInput);
        waitForElement();
        priceFromInput.clear();
        priceFromInput.click();
        priceFromInput.sendKeys(priceFromStr);
        waitForElement();
        validateDisplayedItems(priceToInput);
        priceToInput.clear();
        priceToInput.click();
        priceToInput.sendKeys(priceToStr);
        waitForElement();
        assertTrue("Invalid url!",driver.getCurrentUrl().contains("buyNew=1&offerTypeBuyNow=1&state=7&price_from=500&price_to=4000"));

    }
    public void validateDisplayedItems(WebElement element){
        assertTrue("New item filter isn't displayed! ", element.isDisplayed());
        assertTrue("New item filter isn't enabled! ", element.isEnabled());
    }
    public void waitForElement() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("opbox-listing")));
        Thread.sleep(2000);
    }
    public void getQueryResults() throws IOException{
        WriteFileUtil ob = new WriteFileUtil();
        List<WebElement> productName = driver.findElements(By.xpath("//section/article/div/div/h2/a"));
        List<WebElement> price = driver.findElements(By.xpath("//section/article/div/div/div/div/span/span"));
        List<WebElement> priceWithDelivery = driver.findElements(By.xpath("//section/article/div/div/div/div/div/span/span/span"));
        List<WebElement> utilityState = driver.findElements(By.xpath("//section/article/div/div/div/dl/dd/span"));
        List<WebElement> imageUrl = driver.findElements(By.xpath("//section/article/div/div/a/img"));
        ob.fillXmlFile(productName,price,priceWithDelivery,utilityState,imageUrl);
    }
}
