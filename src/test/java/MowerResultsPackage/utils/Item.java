package MowerResultsPackage.utils;

import com.sun.xml.internal.txw2.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Ja on 03.02.2017.
 */
@XmlRootElement
public class Item {

    private String productName;
    private String price;
    private String utilityState;
    private String priceWithDelivery;
    private String imageUrl;

    public String getProductName(){
        return productName;
    }
    @XmlElement
    public void setProductName(String productName){
        this.productName = productName;
    }
    public String getPrice(){
        return price;
    }
    @XmlElement
    public void setPrice(String price){
        this.price = price;
    }
    public String getPriceWithDelivery(){
        return priceWithDelivery;
    }
    @XmlElement
    public void setPriceWithDelivery(String priceWithDelivery){
        this.priceWithDelivery = priceWithDelivery;
    }
    public String getUtilityState(){
        return utilityState;
    }
    @XmlElement
    public void setUtilityState(String utilityState){
        this.utilityState = utilityState;
    }
    public String getImageUrl(){
        return imageUrl;
    }
    @XmlElement
    public void setImageUrl(String imageUrl){
        this.imageUrl = imageUrl;
    }
}
