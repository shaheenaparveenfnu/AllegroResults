package MowerResultsPackage.utils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ja on 03.02.2017.
 */
@XmlRootElement(name = "items")
@XmlAccessorType(XmlAccessType.FIELD)
public class ItemsDatabaseList {

    @XmlElement(name = "item")
    List<Item> items = new ArrayList<Item>();

    public List<Item> getItems(){
        return items;
    }
    public void setItems(List<Item> items) {
        this.items= items;
    }
}
